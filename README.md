<p align="center">
<img
    style="display: block;
           margin-left: auto;
           margin-right: auto;
           width:60%"
    src="./index_logo.png"
    alt="Our logo">
</img>
</p>

## About

Trestle is a simple tool which allows you to take your beloved sticker packages from Telegram or Signal and port them over to your Matrix client.

- [About](#about)
- [Installation](#installation)
- [Usage](#usage)
- [Contributing](#contributing)


## Installation
TBA

## Usage
TBA

## Contributing
TBA
