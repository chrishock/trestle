# A notepad to keep some ideas and tech in mind

-> sticker converter
-> python

### Import signal sticker programmatically

To import a Signal sticker programmatically, you can use the `signal-sticker-tool` package, which allows you to create, upload, and download Signal stickers [pypi.org](https://pypi.org/project/signal-sticker-tool/). To upload a sticker pack, you first need to authenticate by "borrowing" credentials from an already logged-in Signal Desktop client, then create a directory with the image files and a `stickers.yaml` file containing the sticker pack definition. Once you have the files in place, you can use the command `$ signal-sticker-tool upload` to upload the sticker pack to Signal [pypi.org](https://pypi.org/project/signal-sticker-tool/).

Keep in mind that as of now, Signal does not support animated stickers [reddit.com](https://www.reddit.com/r/signal/comments/l3bi3e/how_to_import_personal_stickers_or_sticker_packs/). If you want to import Telegram stickers to Signal, you can use the Telesignal bot, which allows you to easily import non-animated Telegram stickers to Signal [reddit.com](https://www.reddit.com/r/signal/comments/ktrics/telesignal_easily_import_telegram_stickers_to/).


To create a Signal sticker pack without uploading it, follow these steps:

1.  Prepare your images: Ensure that your images are saved in PNG, WebP, or APNG format and are under 300KB in size. Add a transparent background and a white border to improve legibility on dark mode [reddit.com](https://www.reddit.com/r/signal/comments/kxa631/a_comprehensive_guide_to_find_and_create_stickers/).
    
2.  Create a directory for your sticker pack and place the image files in it.
    
3.  Create a `stickers.yaml` file in the same directory as your image files. The `stickers.yaml` file should include a metadata header (dictionary) with the pack title, author, and cover image file, as well as a list of dictionaries for each sticker with the image file and associated emoji [github.com](https://github.com/ittner/signal-sticker-tool/blob/master/README.md). Here's an example of the `stickers.yaml` file structure:
    

`meta:   title: "Sticker Pack Title"   author: "Author Name"   cover: "cover.webp" stickers:   - file: "sticker_01.webp"     chr: "😀"   - file: "sticker_02.webp"     chr: "😁"`

[

](https://www.phind.com/search?cache=d2d9be90-62f9-4bab-978f-6801d750d1cc)

4.  (Optional) Run `$ signal-sticker-tool init --title "Sticker Pack Title" --author "Author Name"` to automatically generate a `stickers.yaml` file from the image files in the directory [github.com](https://github.com/ittner/signal-sticker-tool/blob/master/README.md).
    
5.  (Optional) Generate a preview of your sticker pack by running `$ signal-sticker-tool preview`. This will create a `preview.html` file in the pack directory, which you can open in your browser to view the stickers and their associated emojis [github.com](https://github.com/ittner/signal-sticker-tool/blob/master/README.md).
    

At this point, you have created a Signal sticker pack without uploading it. If you decide to upload the pack later, you can follow the instructions in the [github.com](https://github.com/ittner/signal-sticker-tool/blob/master/README.md) README.
