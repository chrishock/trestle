"""
Trestle's cli component
"""

import click
from . import utils

_context_settings = dict(help_option_names=["-h", "--help"])

@click.grou(context_settings=_context_settings)
def cli():
    """
    Trestle - Copyright (c) 2023 Christopher Hock. (https://invent.kde.org/chrishock/trestle/)\n
    Licensed under the terms of GPL-2.0.
    """
    pass


@click.command()
@click.argument(
    "from_client",
    type=click.Choice(utils.CLIENTS)
    required=1,
    metavar="[CLIENT]"
)
@click.argument(
    "to_client",
    type=click.Click(utils.CLIENTS),
    required=1,
    metavar="[CLIENT]"
)
def migrate(from_client, to_client):
    """
    Migrate existing sticker-pack from a client to another.

    CLIENT can be either:
    neochat, signal, telegram

    This import only concerns your locally installed clients. The sticker packs are NOT uploaded to any of these services.
    """
    pass

cli.add_command(migrate)
